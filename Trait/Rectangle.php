<?php

//include_once("Shape.php");
include_once("Displayer.php");

class Rectangle extends Shape{

    use Displayer;

    public $length;
    public $width;

    public function __construct($l,$w){
        $this->length = $l;
        $this->width = $w;
    }
        public function area()
    {
        return "Area of Rectangle: ".$this->length*$this->width;
    }

    // public function display(){
    //     echo "Area of Rectangle is: ".$this calculateArea();
    // }
}
?>