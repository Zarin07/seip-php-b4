
<!-- In the example below, $apple and $banana are instances of the class Fruit: -->


<?php

class Fruit {
  // Properties
  public $name;
  public $color;

  // Methods
  function set_name($name) {
    $this->name = $name;
  }
  function get_name() {
    return $this->name;
  }
}


