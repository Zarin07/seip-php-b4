
<!-- In the example below, $name, $age and $color are instances of the class Fruit: -->


<?php

class Dog {
  // Properties
  public $name;
  public $age;
  public $color;

  // Methods
  function set_name($name) {
    $this->name = $name;
  }
  function get_name() {
    return $this->name;
  }
  function set_age($age) {
    $this->age = $age;
  }
  function get_age() {
    return $this->age;
  }
  function set_color($color) {
    $this->color = $color;
  }
  function get_color() {
    return $this->color;
  }
}







?>